/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboles;

/**
 *
 * @author enrique
 */
public class Nodo<T> {
    private T dato;
    private Nodo<T> izquierdo;
    private Nodo<T> derecho;
    
    public Nodo(T dato) {
        this.dato = dato;
        this.izquierdo = null;
        this.derecho = null;
    }
    
    public Nodo(T dato, Nodo derecho, Nodo izquierdo) {
        this.dato = dato;
        this.derecho = derecho;
        this.izquierdo = izquierdo;
    }
    
    
    @Override
    public String toString() {
        return "Dato: " + getDato().toString();
    }

    /**
     * @return the dato
     */
    public T getDato() {
        return dato;
    }

    /**
     * @param dato the dato to set
     */
    public void setDato(T dato) {
        this.dato = dato;
    }

    /**
     * @return the izquierdo
     */
    public Nodo<T> getIzquierdo() {
        return izquierdo;
    }

    /**
     * @param nodo the izquierdo to set
     */
    public void setIzquierdo(Nodo<T> nodo) {
        this.izquierdo = nodo;
    }

    /**
     * @return the derecho
     */
    public Nodo<T> getDerecho() {
        return derecho;
    }

    /**
     * @param nodo the derecho to set
     */
    public void setDerecho(Nodo<T> nodo) {
        this.derecho = nodo;
    }
}
