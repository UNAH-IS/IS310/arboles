/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboles;

/**
 *
 * @author enrique
 */
public class ArbolBinario<T> {
    protected Nodo<T> raiz;
    
    public ArbolBinario() {
        this.raiz = null;
    }
    
    public ArbolBinario(Nodo raiz) {
        this.raiz = raiz;
    }
    
    public ArbolBinario(T dato) {
        this.raiz = new Nodo<T>(dato);
    }
    
    public boolean estaVacio() {
        return this.raiz == null;
    }
    
    public boolean esRaiz(Nodo n) {
        return n == raiz;
    }
    
    public boolean esHoja(Nodo n) {
        return n.getDerecho() == null && n.getIzquierdo() == null;
    }
    
    public void preorden() {
        preorden(this.raiz);
    }
    
    public void preorden(Nodo n) {
        if (n != null) {
            visitar(n);
            preorden(n.getIzquierdo());
            preorden(n.getDerecho());
        }
    }

    public void enOrden() {
        enOrden(this.raiz);
    }
    
    public void enOrden(Nodo n) {
        if (n != null) {
            enOrden(n.getIzquierdo());
            visitar(n);
            enOrden(n.getDerecho());
        }
    }
    
    public void postOrden() {
        postorden(this.raiz);
    }
    
    public void postorden(Nodo n) {
        if (n != null) {
            postorden(n.getIzquierdo());
            postorden(n.getDerecho());
            visitar(n);
        }
    }
    
    public void visitar(Nodo n) {
        System.out.println(n);
    }

    public int conteoNodos() {
        return conteoNodos(this.raiz);
    }
    
    // Funcion recursiva para definir cuantos nodos hay en el arbol
    public int conteoNodos(Nodo n) {
        if (n == null) {
            return 0;
        } else {
            return 1 + conteoNodos(n.getIzquierdo()) + conteoNodos(n.getDerecho());
        }
    }
}
