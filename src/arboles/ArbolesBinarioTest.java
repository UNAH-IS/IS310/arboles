/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboles;

/**
 *
 * @author enrique
 */
public class ArbolesBinarioTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Nodo<Integer> nodo0 = new Nodo<>(10);
        Nodo<Integer> nodo1 = new Nodo<>(20);
        Nodo<Integer> nodo2 = new Nodo<>(30);
        Nodo<Integer> nodo3 = new Nodo<>(40);
        Nodo<Integer> nodo4 = new Nodo<>(50);
        Nodo<Integer> nodo5 = new Nodo<>(60);
        
        ArbolBinario<Integer> arbol = new ArbolBinario<>(nodo0);
        
        nodo0.setIzquierdo(nodo1);
        nodo0.setDerecho(nodo2);
        nodo1.setIzquierdo(nodo4);
        nodo1.setDerecho(nodo3);
        nodo4.setIzquierdo(nodo5);
        
        System.out.println("Busqueda preorden del arbol...");
        arbol.preorden();
        
        System.out.println("Busqueda en orden del arbol...");
        arbol.enOrden();
        
        System.out.println("Busqueda postorden del arbol");
        arbol.postOrden();
        
        System.out.printf("Hay %d nodos%n", arbol.conteoNodos());
    }
    
}
